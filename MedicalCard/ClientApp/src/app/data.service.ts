import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Patient } from './interfaces';
import { Card } from './interfaces';
import { Doctor } from './interfaces';
import { CardRecord } from './interfaces';

@Injectable({
  providedIn: 'root'
})

// CRUD services for api/Patients
export class PatientsDataService {

  private url = 'https://localhost:44312/api/Patients';

  constructor(private http: HttpClient) {
  }

  getPatients() {
    return this.http.get(this.url);
  }

  getPatient(id: number) {
    return this.http.get(this.url + '/' + id);
  }

  createPatient(patient: Patient) {
    return this.http.post(this.url, patient);
  }

  updatePatient(patient: Patient) {
    return this.http.put(this.url + '/' + patient.id, patient);
  }

  deletePatient(id: number) {
    return this.http.delete(this.url + '/' + id);
  }
}


@Injectable({
  providedIn: 'root'
})
// CRUD services for api/CardsRecords
export class CardRecordsDataServices {

  private url = 'https://localhost:44312/api/CardRecords/';

  constructor(private http: HttpClient) {
  }

  getCardRecords() {
    return this.http.get(this.url);
  }

  getCardRecordsByUserId(userId: number) {
    return this.http.get(this.url + userId + '/ByUserId');
  }

  createCardRecord(card: Card) {
    return this.http.post(this.url, card);
  }

  // ajax query
  getDoctors(id: number) {
    return this.http.get(this.url + '/' + id);
  }
}
