export class Patient {
  constructor(
    public id?: number,
    public iin?: string,
    public fullName?: string,
    public adress?: string,
    public telephoneNumber?: string
  ) { }
}
export class Card {
  constructor(
    public id?: number,
    public patient?: Patient
  ) { }
}
export class Doctor {
  constructor(
    public id?: number,
    public fullName?: string,
    public speciality?: string
  ) { }
}
export class CardRecord {
  constructor(
    public id?: number,
    public card?: Card,
    public doctor?: Doctor,
    public diagnosis?: string,
    public complaints?: string,
    public visitDate?: Date
  ) { }
}
