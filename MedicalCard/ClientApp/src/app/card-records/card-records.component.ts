import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {CardRecord} from '../interfaces';
import {PatientsDataService} from '../data.service';
import {CardRecordsDataServices} from '../data.service';

@Component({
  selector: 'app-card-records',
  templateUrl: './card-records.component.html',
  styleUrls: ['./card-records.component.css'],
  providers: [PatientsDataService]
})
export class CardRecordsComponent implements OnInit {
  cardrecords: CardRecord[];
  patientId: number;

  constructor(
    private cardRecordsDataServices: CardRecordsDataServices,
    private patientsDataService: PatientsDataService,
    private router: Router,
    activeRoute: ActivatedRoute)
   {
     this.patientId = Number.parseInt(activeRoute.snapshot.params["id"])
   }

  ngOnInit() {
    this.loadCardRecords(this.patientId); 	// загрузка данных при старте компонента
  }
  // получаем данные через сервис
  loadCardRecords(userId: number) {
    this.cardRecordsDataServices.getCardRecordsByUserId(userId)
      .subscribe((data: CardRecord[]) => this.cardrecords = data);
  }

  onBack() {
    this.router.navigate(['/'])
  }
}

