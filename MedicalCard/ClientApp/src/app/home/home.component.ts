import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {Patient} from '../interfaces';
import {PatientsDataService} from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  patient: Patient = new Patient();   // изменяемый пациент
  patients: Patient[];                // массив пациентов
  tableMode = true;

  constructor(
    private patientsDataService: PatientsDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadPatients(); 	// загрузка данных при старте компонента
  }
  // получаем данные через сервис
  loadPatients() {
    this.patientsDataService.getPatients()
      .subscribe((data: Patient[]) => this.patients = data);
  }
  // сохранение данных
  save() {
    if (this.patient.id == null) {
      this.patientsDataService.createPatient(this.patient)
        .subscribe((data: Patient) => this.patients.push(data));
    } else {
      this.patientsDataService.updatePatient(this.patient)
        .subscribe(data => this.loadPatients());
    }
    this.cancel();
  }
  editPatient(p: Patient) {
    this.patient = p;
  }
  cancel() {
    this.patient = new Patient();
    this.tableMode = true;
  }
  delete(p: Patient) {
    this.patientsDataService.deletePatient(p.id)
      .subscribe(data => this.loadPatients());
  }
  add() {
    this.cancel();
    this.tableMode = false;
  }
  openCard(p: Patient) {
    this.router.navigate(['card-records', `${p.id}`, 'ByUserId'])
  }
}
