import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {CardRecordsComponent} from './card-records/card-records.component';

const routes: Routes = [
  {path: 'patients', component: HomeComponent},
  {path: 'card-records/:id/ByUserId', component: CardRecordsComponent},
  {path: '', redirectTo: 'patients', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
