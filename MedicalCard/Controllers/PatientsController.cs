﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalCard.Models;
using System.Web.Http.Cors;

namespace MedicalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController : ControllerBase
    {
        private readonly MedicalCardContext _context;

        public PatientsController(MedicalCardContext context)
        {
            _context = context;
        }

        // GET: api/Patients
        [HttpGet]
        public IEnumerable<Patient> Getpatients()
        {
            return _context.patients;
        }

        // GET: api/Patients/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPatient([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patient = await _context.patients.FindAsync(id);

            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        // PUT: api/Patients/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPatient([FromRoute] int id, [FromBody] Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patient.id)
            {
                return BadRequest();
            }

            _context.Entry(patient).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Patients
        [HttpPost]
        public async Task<IActionResult> PostPatient([FromBody] Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cardExist = await _context.Cards.Where(c => c.patient.id == patient.id).ToListAsync();
            if (cardExist.Count < 1)
            {
                _context.patients.Add(patient);
                var newPatientCard = new Card();
                newPatientCard.patient = patient;
                _context.Cards.Add(newPatientCard);
            }
            else
            {
                _context.patients.Add(patient);
            }
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPatient", new { id = patient.id }, patient);
        }

        // DELETE: api/Patients/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePatient([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patient = await _context.patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }

            var cardExist = await _context.Cards.Where(c => c.patient.id == id).ToListAsync();
            if (cardExist.Count > 0)
            {
                foreach (Card removeCard in cardExist)
                {
                    var cardRecordsExist = await _context.cardRecords.Where(c => c.card.id == removeCard.id).ToListAsync();
                    foreach (CardRecords removeCardRecord in cardRecordsExist)
                    {
                        _context.cardRecords.Remove(removeCardRecord);
                    }
                    _context.Cards.Remove(removeCard);
                }                
                _context.patients.Remove(patient);
            }
            else
            {
                _context.patients.Remove(patient);
            }
            await _context.SaveChangesAsync();

            return Ok(patient);
        }

        private bool PatientExists(int id)
        {
            return _context.patients.Any(e => e.id == id);
        }
    }
}