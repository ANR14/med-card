﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalCard.Models;

namespace MedicalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardRecordsController : ControllerBase
    {
        private readonly MedicalCardContext _context;

        public CardRecordsController(MedicalCardContext context)
        {
            _context = context;
        }

        // GET: api/CardRecords
        [HttpGet]
        public IEnumerable<CardRecords> GetcardRecords()
        {
            return _context.cardRecords;
        }

        // GET: api/CardRecords/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCardRecords([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cardRecords = await _context.cardRecords.FindAsync(id);

            if (cardRecords == null)
            {
                return NotFound();
            }

            return Ok(cardRecords);
        }
        // GET: api/CardRecords/5/ByUserId
        [HttpGet]
        [Route("{id}/ByUserId")]
        public async Task<IActionResult> GetCardRecordsByUserIs([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cardRecords = await _context.cardRecords.
                Include(r => r.card).
                Include(r => r.doctor).
                Where(r => r.card.patient.id == id).
                ToListAsync();

            if (cardRecords == null)
            {
                return NotFound();
            }

            return Ok(cardRecords);
        }

        // PUT: api/CardRecords/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCardRecords([FromRoute] int id, [FromBody] CardRecords cardRecords)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cardRecords.id)
            {
                return BadRequest();
            }

            _context.Entry(cardRecords).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CardRecordsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CardRecords
        [HttpPost]
        public async Task<IActionResult> PostCardRecords([FromBody] CardRecords cardRecords)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.cardRecords.Add(cardRecords);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCardRecords", new { id = cardRecords.id }, cardRecords);
        }

        // DELETE: api/CardRecords/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCardRecords([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cardRecords = await _context.cardRecords.FindAsync(id);
            if (cardRecords == null)
            {
                return NotFound();
            }

            _context.cardRecords.Remove(cardRecords);
            await _context.SaveChangesAsync();

            return Ok(cardRecords);
        }

        private bool CardRecordsExists(int id)
        {
            return _context.cardRecords.Any(e => e.id == id);
        }
    }
}