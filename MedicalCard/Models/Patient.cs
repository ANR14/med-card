﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MedicalCard.Models
{
    public class Patient
    {
        public int id { get; set; }
        public string IIN { get; set; }
        public string fullName { get; set; }
        public string adress { get; set; }
        public string telephoneNumber { get; set; }
    }
}
