﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalCard.Models
{
    public class CardRecords
    {
        public int id { get; set; }
        public Card card { get; set; }
        public Doctor doctor { get; set; }
        public string diagnosis { get; set; }
        public string complaints { get; set; }
        public DateTime visitDate { get; set; }
    }
}
