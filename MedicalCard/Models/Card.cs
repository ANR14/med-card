﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalCard.Models
{
    public class Card
    {
        public int id { get; set; }
        public Patient patient { get; set; }
    }
}
