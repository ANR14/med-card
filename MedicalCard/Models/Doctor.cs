﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;

namespace MedicalCard.Models
{
    public class Doctor
    {
        public int id { get; set; }
        public string fullName { get; set; }
        public string speciality { get; set; }
    }
}
