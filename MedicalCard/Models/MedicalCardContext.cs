﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalCard.Models
{
    public class MedicalCardContext : DbContext
    {
        public MedicalCardContext()
        {
        }

        public MedicalCardContext(DbContextOptions<MedicalCardContext> options)
            : base(options)
        {
        }

        public DbSet<Card> Cards { get; set; }
        public DbSet<CardRecords> cardRecords { get; set; }
        public DbSet<Doctor> doctors { get; set; }
        public DbSet<Patient> patients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=medicalcarddb;Username=postgres;Password=q1W@e3R$");
        }
    }
}


